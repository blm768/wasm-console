import { resolve } from 'path';
import { defineConfig } from "vite";

export default defineConfig({
  build: {
    target: "esnext",
    polyfillDynamicImport: false,
  },
  resolve: {
    alias: [
      { find: '@', replacement: resolve(__dirname, 'src') },
      { find: '@assets', replacement: resolve(__dirname, 'assets') },
    ],
  },
});
