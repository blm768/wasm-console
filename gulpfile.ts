import { spawn } from "child_process";
import { copyFile } from "fs/promises";
import { join } from "path";
import { chdir, cwd } from "process";

import { parallel, series, TaskFunctionCallback, watch } from "gulp";

function runCmd(cmd: string, ...args: readonly string[]): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const handle = spawn(cmd, args, {
      shell: true, // Needed on Windows because Node can't find "$command.exe" when given "$command".
      stdio: "inherit",
      windowsHide: true,
    });
    handle.on("close", status => {
      if (status === 0) {
        resolve();
      } else {
        reject(new Error(`Process exited with code ${status}.`));
      }
    });
    handle.on("error", err => reject(err));
  });
}

function doChdir<T>(dir: string, func: () => T): T {
  const origDir = cwd();
  chdir(dir);
  try {
    return func();
  } finally {
    chdir(origDir);
  }
}

const cartSrcDirComponents = ["examples", "carts", "default-test-cart"];
const cartSrcDir = join(__dirname, ...cartSrcDirComponents);
const cartSrcDirGlob = cartSrcDirComponents.join('/') + '/**';

export async function testCart(): Promise<void> {
  await doChdir(cartSrcDir, () => runCmd("cargo", "build", "--release", "--target", "wasm32-unknown-unknown"));
  const buildResult = join(__dirname, "target", "wasm32-unknown-unknown", "release", "default_test_cart.wasm");
  await copyFile(buildResult, join(__dirname, "assets", "cart.wasm"));
}

export async function site(cb: TaskFunctionCallback) {
  await runCmd("npx", "tsc");
  await runCmd("vite", "build");
  cb();
}

const all = series(testCart, site);
export default all;

function watchTestCart() {
  watch(
    cartSrcDirGlob,
    (cb: TaskFunctionCallback) => testCart().then(() => cb()).catch(e => cb(e))
  );
}

function viteDev() {
  runCmd("npx", "vite");
}

export const dev = series(
  testCart,
  parallel(
    watchTestCart,
    viteDev,
  )
);

function vitePreview() {
  runCmd("npx", "vite", "preview");
}

export const preview = series(testCart, vitePreview);
