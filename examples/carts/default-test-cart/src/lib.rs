#![no_main]
#![no_std]

use core::arch::wasm32::unreachable;
use core::panic::PanicInfo;

#[panic_handler]
fn handle_panic(_: &PanicInfo) -> ! {
    unreachable()
}

#[link(wasm_import_module = "wasm_console")]
extern "C" {
    fn provide_frame(buf: *const u8, width: usize, height: usize) -> u32;
}

const FRAMEBUFFER: [u8; 16] = [
    255, 255, 255, 255, 0, 0, 0, 255, 0, 0, 0, 255, 255, 255, 255, 255,
];

#[no_mangle]
pub extern "C" fn init() {
    unsafe {
        provide_frame((&FRAMEBUFFER[..]).as_ptr(), 2, 2);
    }
}
