import { Console } from "@/console";

import "./style.css";
import cartUrl from "@assets/cart.wasm?url";

const canvas = document.querySelector<HTMLCanvasElement>("#console-canvas")!
const context = canvas.getContext("2d");
if (context == null) {
    throw new Error("Failed to initialize canvas context");
}
const wasmResponse = await fetch(cartUrl);
const console = await Console.init(wasmResponse, context);
