type CartExports = {
  memory: WebAssembly.Memory,
  init?: () => void,
  release_frame?: () => void,
};

export class Console {
  private constructor(exports: CartExports, context: CanvasRenderingContext2D) {
    this.exports = exports;
    this.context = context;
  }

  public static async init(wasm: Response, context: CanvasRenderingContext2D): Promise<Console> {
    let console: Console;
    const imports: WebAssembly.Imports = {
      wasm_console: {
        provide_frame(offset: number, width: number, height: number): number {
          return console.takeFrame(offset, width, height);
        },
      }
    };

    const { instance } = await WebAssembly.instantiateStreaming(wasm, imports);
    const exports = instance.exports as CartExports;
    console = new Console(exports, context);

    const init = exports.init;
    if (init != undefined) {
      init();
    }

    return console;
  }

  private takeFrame(offset: number, width: number, height: number): number {
    const bufSize = offset + (width * height * 4);
    if (bufSize > Number.MAX_SAFE_INTEGER) {
      return 0;
    }
    const buf = this.exports.memory.buffer.slice(offset, bufSize);
    const array = new Uint8ClampedArray(buf);
    const img = new ImageData(array, width, height);
    // TODO: hold onto the frame until the next paint.
    this.context.canvas.width = width;
    this.context.canvas.height = height;
    this.context.putImageData(img, 0, 0);
    this.releaseFrame();
    return 1;
  }

  private releaseFrame(): void {
    if (this.exports.release_frame != undefined) {
      this.exports.release_frame();
    }
  }

  private exports: CartExports;
  private context: CanvasRenderingContext2D;
}
